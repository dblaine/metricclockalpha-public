package wasden.dblaine.metricclock.alpha;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

public class InfoActivity extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info);
        setCheck();
    }

    public void backPress(View view) {
        Intent mainIntent = new Intent(this, MetricClockActivity.class);
        startActivity(mainIntent);
    }

    public void hourCheck(View view) {
        CheckBox checkbox = (CheckBox) view;

        // Is the view now checked?
        MetricClock.metricHoursInDay(checkbox.isChecked() ? 20 : 10);

       setCheck();
    }

    public void setCheck() {
        CheckBox checkbox = (CheckBox) this.findViewById(R.id.checkBox);

        if (MetricClock.metricHoursInDay() == 10) {
            checkbox.setChecked(false);
            checkbox.setText(R.string.hour10);
        } else {
            checkbox.setChecked(true);
            checkbox.setText(R.string.hour20);
        }
    }
}