package wasden.dblaine.metricclock.alpha;

import java.util.Calendar;

public class MetricClock {
    static public final long millisInDay = 24 * 60 * 60 * 1000;

    static private long hoursInDay = 10;

    static public String metricTime() {
        long millis = sinceMidnight();
        long metricHour = (metricHoursInDay() * millis) / millisInDay;
        long metricMinute = (metricHoursInDay() * 100 * millis) / millisInDay
                - (100 * metricHour);
        long metricSecond = (metricHoursInDay() * 10000 * millis) / millisInDay
                - (10000 * metricHour + 100 * metricMinute);

        return String.format("%02d:%02d:%02d", metricHour, metricMinute, metricSecond);
    }

    static public long metricMillisOnly() {
        long millis = sinceMidnight();
        long metricMillis = (metricHoursInDay() * 10000000 * millis) / millisInDay % 1000;
        return metricMillis;
    }

    static private long metricMillisInDay() {
        return metricHoursInDay() * 100 * 100 * 1000;
    }

    static public long metricHoursInDay() {
        if (hoursInDay <= 0) hoursInDay = 10;
        return hoursInDay;
        //return 10;
    }

    static public void metricHoursInDay(long hours) {
        hoursInDay = hours;
    }

    static private long sinceMidnight() {
        Calendar now = Calendar.getInstance();
        long nowMillis = now.getTimeInMillis();

        Calendar midnight = Calendar.getInstance();
        midnight.set(Calendar.HOUR_OF_DAY, 0);
        midnight.set(Calendar.MINUTE, 0);
        midnight.set(Calendar.SECOND, 0);
        midnight.set(Calendar.MILLISECOND, 0);

        long nowSinceMidnight = (nowMillis - midnight.getTimeInMillis());
        if (nowSinceMidnight < 0) nowSinceMidnight += millisInDay;
        if (nowSinceMidnight >= millisInDay) nowSinceMidnight -= millisInDay;
        return nowSinceMidnight;
    }
    static public long ratioNormalToMetricByMil() {
        return 1000 * millisInDay
            / (metricHoursInDay() * 100 * 100 * 1000);
    }
}
