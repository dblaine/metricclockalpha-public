package wasden.dblaine.metricclock.alpha;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MetricClockActivity extends Activity {

    private Handler updateHandler = new Handler();
    private Boolean running = false;

    private Boolean debugInfoFlag = false;
    private static long debugCount = 0;


    private Runnable updateTask = new Runnable() {
        public void run() {
            long now = SystemClock.uptimeMillis();

            TextView viewmetric = (TextView) findViewById(R.id.timemetric);
            viewmetric.setText(MetricClock.metricTime());

            SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
            TextView viewnormal = (TextView) findViewById(R.id.timenormal);
            viewnormal.setText(format.format(new Date()));

            if (running) {
                long metricDelta = 1000 - MetricClock.metricMillisOnly();
                long delta = MetricClock.ratioNormalToMetricByMil() * metricDelta / 1000
                        + 2000 / MetricClock.metricHoursInDay();
                if (debugInfoFlag) {
                    TextView debugView = (TextView) findViewById(R.id.debugInfo);
                    debugView.setText(String.format("milli %03d, delay %03d, count %d",
                            MetricClock.metricMillisOnly(), delta, debugCount++));
                }
                updateHandler.postAtTime(this, now + delta);
            } else {
                updateHandler.removeCallbacks(this);
            }
        }
    };

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    @Override
    protected void onStart() {
        super.onStart();    //To change body of overridden methods use File | Settings | File Templates.

        running = true;
        long now = SystemClock.uptimeMillis();
        updateHandler.postAtTime(updateTask, now + 1000 / 10);
    }

    @Override
    protected void onStop() {
        super.onStop();    //To change body of overridden methods use File | Settings | File Templates.

        running = false;
        //updateHandler.removeCallbacks(updateTask);
    }

    @Override
    protected void onDestroy() {
        running = false;
        super.onDestroy();    //To change body of overridden methods use File | Settings | File Templates.
        updateHandler.removeCallbacks(updateTask);
    }

    public void infoPress(View view) {
        Intent infoIntent = new Intent(this, InfoActivity.class);
        startActivity(infoIntent);
    }

    public void exitPress(View view) {
        moveTaskToBack(true);
    }
}
